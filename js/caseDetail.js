const paramsStr = window.location.search
const params = new URLSearchParams(paramsStr)
const type = params.get('type')

window.onload = function () {
    $(`#case${type}`).show()
    $(window).resize(() => {
        setBodyPadtop($('#headBox').outerHeight())
    })

    $('#head').load('../page/head.html', () => {
        setBodyPadtop($('#headBox').outerHeight())
    })
    $('#footer').load('../page/footer.html')

    const myNew = getFuwu(type)

    $(`#casebreadTitle${type}`).html(myNew.name)
}

function setBodyPadtop(padtop) {
    $('body').css('padding-top', padtop + 'px')
}

const fuwuList = [
    {
        type: 1,
        name: '光伏储能领域的场景应用'
    },
    {
        type: 2,
        name: '新零售场景应用'
    },
    {
        type: 3,
        name: '数智化转型场景应用'
    },
    {
        type: 4,
        name: '智能充电桩场景应用'
    },
]


function getFuwu(type) {
    let myFuwu = {}
    fuwuList && fuwuList.map(item => {
        if (item.type == type) {
            myFuwu = item
        }
    })

    return myFuwu
}

function menuHandle(jumpUrl) {
    location.href = jumpUrl
}