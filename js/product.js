window.onload = function () {
    $(window).resize(() => {
        setBodyPadtop($('#headBox').outerHeight())
    })

    $('#head').load('../page/head.html', () => {
        setBodyPadtop($('#headBox').outerHeight())
    })
    $('#footer').load('../page/footer.html')
    setProdGuigeCanshu()
    setJianrong()
}

function setBodyPadtop(padtop) {
    $('body').css('padding-top', padtop + 'px')
}


const canshuList = [
    {
        name: '规格尺寸',
        value: 'Mini、Micro、Nano、贴片、异形定制',
    },
    {
        name: 'TAC码白名单',
        value: '支持',
    },
    {
        name: '短信切网',
        value: 'N/A',
    },
    {
        name: '温度范围',
        value: '-40°C - 105C',
    },
    {
        name: '自动切网开关',
        value: '支持',
    },
    {
        name: 'eSIM集成',
        value: '支持',
    },
    {
        name: '湿度范围',
        value: '50 - 90%',
    },
    {
        name: '策略切网',
        value: '支持',
    },
    {
        name: '卡基定制',
        value: '支持',
    },
    {
        name: '强度范围',
        value: '垂直10N',
    },
]

const jianrongList = [
    {
        title: 'Nano加长防研磨',
        imgUrl: '../imgs/prod-img8.png',
    },
    {
        title: '工业版SIM卡座',
        imgUrl: '../imgs/prod-img9.png',
    },
    {
        title: '5*6贴片封装',
        imgUrl: '../imgs/prod-img10.png',
    },
    {
        title: '定制化贴片封装',
        imgUrl: '../imgs/prod-img11.png',
    },
    {
        title: 'PCBA板',
        imgUrl: '../imgs/prod-img12.png',
    },
]

function setProdGuigeCanshu() {
    canshuList && canshuList.map(item => {
        $('#prodGuigeCanshu').append(`
            <div class="canshu-item">
                <span>${item.name}：</span>
                <span>${item.value}</span>
            </div>
        `)
    })
}

function setJianrong() {
    jianrongList && jianrongList.map(item => {
        $('#jianrongImgBox').append(`
            <div>
                <img src="${item.imgUrl}" alt="">
                <div>${item.title}</div>
            </div>
        `)
    })
}

function menuHandle(url) {
    location.href = url
}