const paramsStr = window.location.search
const params = new URLSearchParams(paramsStr)
const type = params.get('type')

window.onload = function () {
    $(`#type${type}`).show()
    $(window).resize(() => {
        setBodyPadtop($('#headBox').outerHeight())
    })

    $('#head').load('../page/head.html', () => {
        setBodyPadtop($('#headBox').outerHeight())
    })
    $('#footer').load('../page/footer.html')

    const myNew = getFuwu(type)

    $(`#breadTitle${type}`).html(myNew.name)
}

function setBodyPadtop(padtop) {
    $('body').css('padding-top', padtop + 'px')
}

const fuwuList = [
    {
        type: 1,
        name: '客户端软件与终端SDK'
    },
    {
        type: 2,
        name: '全讯IOT平台'
    },
]


function getFuwu(type) {
    let myFuwu = {}
    fuwuList && fuwuList.map(item => {
        if (item.type == type) {
            myFuwu = item
        }
    })

    return myFuwu
}

function menuHandle(jumpUrl) {
    location.href = jumpUrl
}