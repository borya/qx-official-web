window.onload = function () {
    $(window).resize(() => {
        setBodyPadtop($('#headBox').outerHeight() - 20)
    })

    $('#head').load('../page/head.html', () => {
        setBodyPadtop($('#headBox').outerHeight() - 20)
    })
    $('#footer').load('../page/footer.html')

    var mySwiper = new Swiper('.swiper', {
        autoplay: {
            delay: 3000,
            stopOnLastSlide: false,
            disableOnInteraction: true,
        },
        resizeObserver: true,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    })

    setBanner()

    setAnimate('formleft', 'animate__backInLeft')
    setAnimate('formright', 'animate__backInRight')
    setAnimate('formin', 'animate__fadeIn', () => {
        $('.counter').countUp()
    })
}

const bannerList = [
    '../imgs/banner2.jpg',
    '../imgs/banner3.jpg',
]

function setBanner() {
    bannerList && bannerList.map(item => {
        $('.swiper-wrapper').append(`
            <div class="swiper-slide">
                <img style="width:100%;" src="${item}">
            </div>
        `)
    })
}

function setBodyPadtop(padtop) {
    $('body').css('padding-top', padtop + 'px')
}

function setAnimate(boxClass, animateClass, callback = () => { }) {
    new WOW({
        boxClass,
        animateClass,
        offset: 0,
        mobile: true,
        live: true,
    }).init()
    callback()
}

function jumpHandle(url) {
    location.href = url
}