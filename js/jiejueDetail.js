const paramsStr = window.location.search
const params = new URLSearchParams(paramsStr)
const type = params.get('type')

window.onload = function () {
    $(`#case${type}`).show()
    $(window).resize(() => {
        setBodyPadtop($('#headBox').outerHeight())
    })

    $('#head').load('../page/head.html', () => {
        setBodyPadtop($('#headBox').outerHeight())
    })
    $('#footer').load('../page/footer.html')

    const myNew = getJiejue(type)

    $(`#jiejueTitle${type}`).html(myNew.name)
}

function setBodyPadtop(padtop) {
    $('body').css('padding-top', padtop + 'px')
}

const jiejueList = [
    {
        type: 1,
        name: '移动场景高可靠数据传输方案'
    },
    {
        type: 2,
        name: '小微企业高可靠网络覆盖解决方案'
    },
    {
        type: 3,
        name: '移动智能终端/安全帽'
    },
    {
        type: 4,
        name: '工业音视频作业调度系统'
    },
    {
        type: 5,
        name: '智慧机房管理系统'
    },
    {
        type: 6,
        name: '智能巡检系统'
    },
]


function getJiejue(type) {
    let myFuwu = {}
    jiejueList && jiejueList.map(item => {
        if (item.type == type) {
            myFuwu = item
        }
    })

    return myFuwu
}

function menuHandle(jumpUrl) {
    location.href = jumpUrl
}