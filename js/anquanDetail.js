const paramsStr = window.location.search
const params = new URLSearchParams(paramsStr)
const type = params.get('type')

window.onload = function () {
    $(`#chanp${type}`).show()

    $(window).resize(() => {
        setBodyPadtop($('#headBox').outerHeight())
    })

    $('#head').load('../page/head.html', () => {
        setBodyPadtop($('#headBox').outerHeight())
    })
    $('#footer').load('../page/footer.html')

}

function setBodyPadtop(padtop) {
    $('body').css('padding-top', padtop + 'px')
}

function menuHandle(jumpUrl) {
    location.href = jumpUrl
}