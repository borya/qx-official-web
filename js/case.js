window.onload = function () {
    $(window).resize(() => {
        setBodyPadtop($('#headBox').outerHeight())
    })

    $('#head').load('../page/head.html', () => {
        setBodyPadtop($('#headBox').outerHeight())
    })
    $('#footer').load('../page/footer.html')

    setAlItem()
}

function setBodyPadtop(padtop) {
    $('body').css('padding-top', padtop + 'px')
}

const alList = [
    {
        type: 1,
        title: '光伏储能领域的场景应用',
        imgUrl: '../imgs/yyal-img1.jpg',
        content: '光伏储能常用在农乡地带、山林间通过光伏配电房、网络终端、监控摄像头等设备，为用户提供电池寿命、温度、湿度、环境等数据传输以及监控运维等工作。全讯高可靠的网络连接方案可提供可靠的网络支持，保证数据传输的稳定性，同时可通过全讯IOT平台进行流量监控、切换策略、网络运维等服务。'
    },
    {
        type: 4,
        title: '智能充电桩场景应用',
        imgUrl: '../imgs/yyal-img2.jpg',
        content: '新零售行业是指传统零售与数字技术相结合，利用互联网、大数据、人工智能等技术手段进行创新和升级的零售业态。针对自助售货机场景，配置全讯多网融合物联网卡技术服务，将三大运营商网络融合，保证网络实时在线，免去用户下单、支付时无信号、无网络等情况。'
    },
    {
        type: 2,
        title: '新零售场景应用',
        imgUrl: '../imgs/yyal-img3.jpg',
        content: '新零售行业是指传统零售与数字技术相结合，利用互联网、大数据、人工智能等技术手段进行创新和升级的零售业态。针对自助售货机场景，配置全讯多网融合物联网卡技术服务，将三大运营商网络融合，保证网络实时在线，免去用户下单、支付时无信号、无网络等情况。'
    },
    {
        type: 3,
        title: '数智化转型场景应用',
        imgUrl: '../imgs/yyal-img4.jpg',
        content: '某市偏远地区河道管理数字化转型，引用了4G智能终端+全讯多网融合物联网卡，搭配太阳能监控系统。为河道监控提供稳定的网络支持、减少网络断点，保障数据传输及图像采集。数字化管理河道可减少巡查、上报等人力成本，提高实效性，成本可控。'
    },
]

function setAlItem() {
    alList && alList.map(item => {
        $('#yyalItemBox').append(`
        <div class="yyal-item">
            <div class="yyal-img-box" onclick="jumpHandle(${item.type})">
                <img src="${item.imgUrl}" alt="">
            </div>
            <div class="case-title" onclick="jumpHandle(${item.type})">${item.title}</div>
            <div class="case-cotiner">${item.content}</div>
        </div>
        `)
    })
}

function jumpHandle(type) {
    location.href = `/page/caseDetail.html?type=${type}`
}