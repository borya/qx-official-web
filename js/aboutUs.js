window.onload = function () {
    $(window).resize(() => {
        setBodyPadtop($('#headBox').outerHeight())
    })

    $('#head').load('../page/head.html', () => {
        setBodyPadtop($('#headBox').outerHeight())
    })
    $('#footer').load('../page/footer.html')

    AMapLoader.load({
        key: "4e7b2cb1bfce119de8c8a2f9c6e88c60",
        version: "2.0",
    }).then((AMap) => {
        const map = new AMap.Map("container", {
            zoom: 16,
            center: [117.145834, 36.656672],
            mapStyle: "amap://styles/normal",
            viewMode: "2D",
        });
        const marker = new AMap.Marker({
            position: new AMap.LngLat(117.145834, 36.656672),
            title: "全讯物联技术（济南）有限公司",
        });
        marker.on("click", function (e) {
            window.open('https://uri.amap.com/marker?position=117.145834,36.656672', '_blank')
        });
        var mapContent = [
            "<div>全讯物联</div>",
        ]

        var infoWindow = new AMap.InfoWindow({
            content: mapContent.join(""),
            offset: new AMap.Pixel(0, -32)
        });
        map.add(marker);
        infoWindow.open(map, new AMap.LngLat(117.145834, 36.656672));
    }).catch((e) => {
        console.error(e)
    });
}

function setBodyPadtop(padtop) {
    $('body').css('padding-top', padtop + 'px')
}

window._AMapSecurityConfig = {
    securityJsCode: "194dbef223d8cbe76bcbff4de2fbef31",
};

function jumpHandle(url) {
    location.href = url
}