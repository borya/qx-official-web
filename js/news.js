window.onload = function () {
    $(window).resize(() => {
        setBodyPadtop($('#headBox').outerHeight())
    })

    $('#head').load('../page/head.html', () => {
        setBodyPadtop($('#headBox').outerHeight())
    })
    $('#footer').load('../page/footer.html')

    setNewsItem()
}

function setBodyPadtop(padtop) {
    $('body').css('padding-top', padtop + 'px')
}

const newsList = [
    {
        id: 1,
        imgUrl: 'https://www.flymodem.com/wp-content/uploads/2023/12/大会主题-768x512.jpg',
        title: '多网融合技术创新，飞猫高可靠网络解决方案入选2024AIoT产业图谱',
        content: '“中国 AIoT 产业年会暨 2024  年智能产业前瞻洞察大典”今日在深圳凯悦酒店隆重召开。以“先知、先行、先机”为主题，本次大会旨在探讨和展望AIoT产业的发展趋势和未来。飞猫携多网融合创新技术受邀与行业领袖们分享创新成果，探讨如何应对AIOT产业当前面临的挑战。',
        time: '2023年 12月 15日',
        publishUser: '全讯物联',
    },
    {
        id: 1,
        imgUrl: 'https://www.flymodem.com/wp-content/uploads/2023/12/大会主题-768x512.jpg',
        title: '多网融合技术创新，飞猫高可靠网络解决方案入选2024AIoT产业图谱',
        content: '“中国 AIoT 产业年会暨 2024  年智能产业前瞻洞察大典”今日在深圳凯悦酒店隆重召开。以“先知、先行、先机”为主题，本次大会旨在探讨和展望AIoT产业的发展趋势和未来。飞猫携多网融合创新技术受邀与行业领袖们分享创新成果，探讨如何应对AIOT产业当前面临的挑战。',
        time: '2023年 12月 15日',
        publishUser: '全讯物联',
    },
    {
        id: 1,
        imgUrl: 'https://www.flymodem.com/wp-content/uploads/2023/12/大会主题-768x512.jpg',
        title: '多网融合技术创新，飞猫高可靠网络解决方案入选2024AIoT产业图谱',
        content: '“中国 AIoT 产业年会暨 2024  年智能产业前瞻洞察大典”今日在深圳凯悦酒店隆重召开。以“先知、先行、先机”为主题，本次大会旨在探讨和展望AIoT产业的发展趋势和未来。飞猫携多网融合创新技术受邀与行业领袖们分享创新成果，探讨如何应对AIOT产业当前面临的挑战。',
        time: '2023年 12月 15日',
        publishUser: '全讯物联',
    },
    {
        id: 1,
        imgUrl: 'https://www.flymodem.com/wp-content/uploads/2023/12/大会主题-768x512.jpg',
        title: '多网融合技术创新，飞猫高可靠网络解决方案入选2024AIoT产业图谱',
        content: '“中国 AIoT 产业年会暨 2024  年智能产业前瞻洞察大典”今日在深圳凯悦酒店隆重召开。以“先知、先行、先机”为主题，本次大会旨在探讨和展望AIoT产业的发展趋势和未来。飞猫携多网融合创新技术受邀与行业领袖们分享创新成果，探讨如何应对AIOT产业当前面临的挑战。',
        time: '2023年 12月 15日',
        publishUser: '全讯物联',
    },
    {
        id: 1,
        imgUrl: 'https://www.flymodem.com/wp-content/uploads/2023/12/大会主题-768x512.jpg',
        title: '多网融合技术创新，飞猫高可靠网络解决方案入选2024AIoT产业图谱',
        content: '“中国 AIoT 产业年会暨 2024  年智能产业前瞻洞察大典”今日在深圳凯悦酒店隆重召开。以“先知、先行、先机”为主题，本次大会旨在探讨和展望AIoT产业的发展趋势和未来。飞猫携多网融合创新技术受邀与行业领袖们分享创新成果，探讨如何应对AIOT产业当前面临的挑战。',
        time: '2023年 12月 15日',
        publishUser: '全讯物联',
    },
    {
        id: 1,
        imgUrl: 'https://www.flymodem.com/wp-content/uploads/2023/12/大会主题-768x512.jpg',
        title: '多网融合技术创新，飞猫高可靠网络解决方案入选2024AIoT产业图谱',
        content: '“中国 AIoT 产业年会暨 2024  年智能产业前瞻洞察大典”今日在深圳凯悦酒店隆重召开。以“先知、先行、先机”为主题，本次大会旨在探讨和展望AIoT产业的发展趋势和未来。飞猫携多网融合创新技术受邀与行业领袖们分享创新成果，探讨如何应对AIOT产业当前面临的挑战。',
        time: '2023年 12月 15日',
        publishUser: '全讯物联',
    },
    {
        id: 1,
        imgUrl: 'https://www.flymodem.com/wp-content/uploads/2023/12/大会主题-768x512.jpg',
        title: '多网融合技术创新，飞猫高可靠网络解决方案入选2024AIoT产业图谱',
        content: '“中国 AIoT 产业年会暨 2024  年智能产业前瞻洞察大典”今日在深圳凯悦酒店隆重召开。以“先知、先行、先机”为主题，本次大会旨在探讨和展望AIoT产业的发展趋势和未来。飞猫携多网融合创新技术受邀与行业领袖们分享创新成果，探讨如何应对AIOT产业当前面临的挑战。',
        time: '2023年 12月 15日',
        publishUser: '全讯物联',
    },
]

function setNewsItem() {
    newsList && newsList.map(item => {
        $('#newsItemBox').append(`
            <div class="news-item" onclick="newsHandle(${item.id})">
                <div>
                    <img style="width:100%;" src="${item.imgUrl}">
                </div>
                <div>
                    <div class="news-item-title">${item.title}</div>
                    <div class="news-item-content">${item.content}</div>
                    <div class="news-item-time">${item.publishUser} / ${item.time}</div>
                </div>
            </div>
        `)
    })
}

function newsHandle(newsId) {
    location.href = `/page/newsDetail.html?newsId=${newsId}`
}