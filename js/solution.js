window.onload = function () {
    $(window).resize(() => {
        setBodyPadtop($('#headBox').outerHeight())
    })

    $('#head').load('../page/head.html', () => {
        setBodyPadtop($('#headBox').outerHeight())
    })
    $('#footer').load('../page/footer.html')
}

function setBodyPadtop(padtop) {
    $('body').css('padding-top', padtop + 'px')
}

function jumpHandle(url) {
    location.href = url
}